package me.ippolitov.car.lib;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Buffers {

    public static ByteBuffer allocateBuffer() {
        return allocateBuffer(128);
    }

    public static ByteBuffer wrap(byte[] array) {
        ByteBuffer buffer = allocateBuffer(array.length).put(array);
        buffer.flip();
        return buffer;
    }

    public static ByteBuffer allocateBuffer(int capacity) {
        return ByteBuffer.allocate(capacity).order(ByteOrder.LITTLE_ENDIAN);
    }

    public static ByteBuffer duplicate(ByteBuffer buffer) {
        return buffer.duplicate().order(buffer.order()); //  JDK-8157013
    }
}
