package me.ippolitov.car.lib;

import android.app.IntentService;
import android.content.Intent;
import android.os.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.Process;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class CarUDPService extends IntentService {

    private static final long TIMEOUT_NO_PACKETS_SENT_MILLIS = 2 * 60 * 1000;  //  2 minutes
    private static final long TIMEOUT_PACKET_LIFESPAN = 30 * 1000; //  30 seconds
    private static final int QUEUE_SIZE_LIMIT = 50;

    private static final Logger LOG = LoggerFactory.getLogger(CarUDPService.class);

    private static final int PORT = 42465;
    private static final List<String> SERVERS = Arrays.asList("car.ippolitov.me");

    private DatagramChannel channel;
    private DataMediator dataMediator;

    private final PacketQueue outgoingQueue = new PacketQueue(QUEUE_SIZE_LIMIT, TIMEOUT_PACKET_LIFESPAN);
    private Selector selector;
    private boolean rebootHackEnabled;
    private long lastTimePacketSent = System.currentTimeMillis();

    public CarUDPService() {
        super("CarUDPService");
        try {
            channel = DatagramChannel.open();
            channel.socket().bind(new InetSocketAddress(0));
            selector = Selector.open();
            channel.configureBlocking(false);
            channel.register(selector, SelectionKey.OP_READ);
        } catch (IOException e) {
            LOG.error("Exception in constructor", e);
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        LOG.debug("Service started");
        try {
            final ByteBuffer buffer = Buffers.allocateBuffer();
            PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
            PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "CarUDPWakeLock");
            wakeLock.setReferenceCounted(false);

            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    selector.select();
                    wakeLock.acquire(5000);
                    try {
                        final Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                        while (iterator.hasNext()) {
                            SelectionKey key = iterator.next();
                            if (key.isValid()) {
                                if (key.channel() != channel) {
                                    throw new RuntimeException("Unexpected channel");
                                }
                                if (key.isReadable()) {
                                    performRead(buffer);
                                }
                                if (key.isWritable()) {
                                    performWrite();
                                }
                            }
                            iterator.remove();
                        }
                    } finally {
                        wakeLock.release();
                    }
                } catch (IOException e) {
                    if (e instanceof UnknownHostException || "Network is unreachable".equals(e.getMessage())) {
                        LOG.error(e.getClass().getSimpleName() + ": " + e.getMessage());
                    } else {
                        LOG.error("Exception in loop", e);
                    }
                    Thread.sleep(2000);
                }
            }
        } catch (Exception e) {
            LOG.error("Unexpected exception", e);
        }
    }

    @Override
    public void onDestroy() {
        LOG.warn("UDP service destroyed!");
        super.onDestroy();
    }

    private void performWrite() throws IOException {
        Packet packet = outgoingQueue.get();
        if (packet != null) {
            if (!sendPacketToAllServers(packet.byteBuffer())) {
                LOG.warn("Packet could not be sent to any servers. Will retry until packet expires in {} seconds", packet.getAge() / 1000);
            } else {
                lastTimePacketSent = System.currentTimeMillis();
                outgoingQueue.remove(packet);
            }
        }
        synchronized (outgoingQueue) {  //  external synchronization to prevent race
            if (outgoingQueue.isEmpty()) {
                channel.register(selector, SelectionKey.OP_READ);
            } else {
                LOG.trace("Queue not empty, still registered for write");
            }
        }
    }

    private void performRead(final ByteBuffer buffer) throws IOException {
        buffer.clear();
        SocketAddress from = channel.receive(buffer);
        if (from == null) {
            LOG.warn("Received nothing");
        } else {
            buffer.flip();
            LOG.trace("Received a packet from {}: {}", from, Utils.debugBufStr(buffer));
            if (checkMagic(buffer)) {
                dataMediator.sendFromUDP(Packet.builder().putData(buffer.array(), Packet.MAGIC.length, buffer.limit()-Packet.MAGIC.length).build());
            } else {
                LOG.warn("Magic mismatch");
            }
        }
    }

    private boolean checkMagic(ByteBuffer buffer) {
        for (int i = 0; i < Packet.MAGIC.length; ++i) {
            if (buffer.array()[i] != Packet.MAGIC[i]) {
                return false;
            }
        }
        return true;
    }

    private boolean sendPacketToAllServers(ByteBuffer packet) throws IOException {
        boolean sentSomewhere = false;
        LOG.debug("Sending packet to servers: {}", Utils.debugBufStr(packet));
        for (String host : SERVERS) {
            InetSocketAddress serverAddress = resolveServerAddress(host);
            boolean sentOk = channel.send(packet.asReadOnlyBuffer(), serverAddress) > 0;
            if (!sentOk) {
                LOG.warn("Packet could not be sent completely to {}:{}", serverAddress.getAddress().getHostAddress(), serverAddress.getPort());
            }
            sentSomewhere |= sentOk;
        }
        return sentSomewhere;
    }

    public void sendPacketWithData(final Packet packet) {
        outgoingQueue.add(packet);
        if (rebootHackEnabled && noPacketsSentForTooLong()) {
            rebootHack();
        }
        try {
            channel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
            selector.wakeup();
        } catch (ClosedChannelException e) {
            LOG.error("Exception when registering channel for write", e);
            throw new RuntimeException(e);
        }
    }


    private void rebootHack() {
        try {
            Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", "reboot" });
            proc.waitFor();
        } catch (Exception ex) {
            LOG.error("Could not reboot", ex);
        }
    }

    private boolean noPacketsSentForTooLong() {
        return System.currentTimeMillis() - lastTimePacketSent > TIMEOUT_NO_PACKETS_SENT_MILLIS;
    }

    private InetSocketAddress resolveServerAddress(String host) throws UnknownHostException {
        LOG.trace("Resolving server host {}", host);
        return new InetSocketAddress(InetAddress.getByName(host), PORT);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new UDPBinder();
    }

    public void setDataMediator(DataMediator dataMediator) {
        this.dataMediator = dataMediator;
    }

    public void setRebootHackEnabled(boolean rebootHackEnabled) {
        this.rebootHackEnabled = rebootHackEnabled;
    }

    public class UDPBinder extends Binder {
        public CarUDPService getService() {
            return CarUDPService.this;
        }
    }
}
