package me.ippolitov.car.lib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class DataMediator {
    private Logger LOG = LoggerFactory.getLogger(DataMediator.class);

    private CarUDPService udpService;
    private List<DataMediatorSink> otherEnds = new ArrayList<>();

    public void sendViaUDP(Packet packet) {
        if (udpService == null) {
            LOG.warn("No UDP service to send packet to!");
        } else {
            udpService.sendPacketWithData(packet);
        }
    }

    public void sendFromUDP(Packet packet) {
        if (otherEnds.isEmpty()) {
            LOG.warn("No other ends to handle packets!");
        } else {
            for (DataMediatorSink otherEnd : otherEnds) {
                if (otherEnd.handlePacket(packet)) {
                    return;
                }
            }
            LOG.warn("No other ends were able to handle the packet!");
        }
    }

    public void setUdpService(CarUDPService udpService) {
        this.udpService = udpService;
    }

    public void addOtherEnd(DataMediatorSink otherEnd) {
        this.otherEnds.add(otherEnd);
    }

    public void removeOtherEnd(DataMediatorSink otherEnd) {
        this.otherEnds.remove(otherEnd);
    }
}
