package me.ippolitov.car.lib;

public interface DataMediatorSink {
    void setDataMediator(DataMediator dataMediator);

    boolean handlePacket(Packet packet);
}
