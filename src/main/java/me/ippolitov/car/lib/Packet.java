package me.ippolitov.car.lib;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Packet {
    static final byte[] MAGIC = getMagic();

    private static byte[] getMagic() {
        return getBytes("CAR");
    }

    private static byte[] getBytes(String str) {
        try {
            return String.valueOf(str).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private final ByteBuffer buffer;
    private long buildTime; // in millis

    private Packet(ByteBuffer buffer, long buildTime) {
        this.buffer = buffer;
        this.buildTime = buildTime;
    }

    public static Builder builder() {
        return new Builder();
    }

    public ByteBuffer byteBuffer() {
        return buffer.slice().order(ByteOrder.LITTLE_ENDIAN);
    }

    public long getAge() {
        return System.currentTimeMillis() - buildTime;
    }
    public int size() {
        return buffer.limit();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Packet packet = (Packet) o;

        return buffer.equals(packet.buffer);
    }

    @Override
    public int hashCode() {
        return buffer.hashCode();
    }

    public static class Builder {
        private ByteBuffer buffer = Buffers.allocateBuffer();

        private Builder() {
        }

        public Builder putData(final byte[] buf, final int offset, final int length) {
            buffer.put(buf, offset, length);
            return this;
        }

        public Builder putData(final ByteBuffer buf) {
            buffer.put(buf);
            return this;
        }

        public Builder putMagic() {
            return putData(MAGIC, 0, MAGIC.length);
        }

        public Builder putKeepaliveMarker() {
            return putData(getBytes("_CAR_keep"));
        }

        private Builder putData(byte[] buf) {
            return putData(buf, 0, buf.length);
        }

        public Packet build() {
            this.buffer.flip();
            Packet result = new Packet(this.buffer, System.currentTimeMillis());
            // .asReadOnlyBuffer();  <-  Can't be used while there are .array() calls
            this.buffer = null;  //  Hacky way to prevent reuse of this builder
            return result;
        }
    }
}
