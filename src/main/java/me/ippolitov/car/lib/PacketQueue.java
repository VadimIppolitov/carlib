package me.ippolitov.car.lib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class PacketQueue {
    private static final Logger LOG = LoggerFactory.getLogger(PacketQueue.class);

    private final int queueSizeLimit;
    private final long timeoutPacketLifespan;
    private final List<Packet> queue;

    public PacketQueue(int queueSizeLimit, long timeoutPacketLifespan) {
        this.queueSizeLimit = queueSizeLimit;
        this.timeoutPacketLifespan = timeoutPacketLifespan;
        queue = new ArrayList<>(queueSizeLimit);
    }

    public synchronized Packet get() {
        purgeStalePackets();
        if (queue.isEmpty()) {
            LOG.warn("No packet in queue");
            return null;
        }
        return queue.get(0);
    }

    public synchronized void remove(Packet packet) {
        for (Iterator<Packet> iterator = queue.iterator(); iterator.hasNext(); ) {
            if (packet == iterator.next()) {
                iterator.remove();
                break;
            }
        }
    }

    public synchronized void add(Packet packet) {
        purgeStalePackets();
        if (isFull()) {
            Packet old = queue.remove(0);
            LOG.warn("Dropped oldest packet due to queue overflow: {}", Utils.debugBufStr(old.byteBuffer()));
        }
        queue.add(packet);
    }

    public synchronized boolean isEmpty() {
        return queue.isEmpty();
    }

    public synchronized boolean isFull() {
        return queue.size() >= queueSizeLimit;
    }

    private synchronized void purgeStalePackets() {
        for (Iterator<Packet> iterator = queue.iterator(); iterator.hasNext(); ) {
            Packet packet = iterator.next();
            long age = packet.getAge();
            if (age > timeoutPacketLifespan) {
                String bufStr = Utils.debugBufStr(packet.byteBuffer());
                LOG.warn("Removed stale packet, was {} seconds old: {}", age / 1000, bufStr);
                iterator.remove();
            }
        }
    }
}
