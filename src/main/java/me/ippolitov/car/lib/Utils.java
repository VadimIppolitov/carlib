package me.ippolitov.car.lib;

import android.annotation.SuppressLint;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

public class Utils {
    public static void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
    }

    @SuppressLint("DefaultLocale")
    public static String debugBufStr(ByteBuffer buffer) {
        String content = simplify(getStringFromBuffer(buffer));
        return String.format("[p=%d, l=%d, c=%d, cont=%s]", buffer.position(), buffer.limit(), buffer.capacity(), content);
    }

    private static String simplify(String string) {
        return string.replaceAll("[^A-Za-z0-9_]", "?");
    }

    public static String getStringFromBuffer(ByteBuffer buffer) {
        try {
            return new String(buffer.array(), buffer.position(), buffer.remaining(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
