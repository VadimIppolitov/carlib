package me.ippolitov.car.lib;

import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.*;

public class PacketQueueTest {
    private final int timeout = 100;
    private PacketQueue q;
    private int seq = 0;

    @Before
    public void init() {
        q = new PacketQueue(3, timeout);
    }

    @Test
    public void testGetNone() {
        assertNull(q.get());
        assertTrue(q.isEmpty());
    }

    @Test
    public void testAddGet() {
        Packet p = newPacket();
        q.add(p);
        assertEquals(p, q.get());
        assertEquals(p, q.get()); //  get does not remove it
        assertFalse(q.isEmpty());
    }

    @Test
    public void testRemoveAlien() {
        q.remove(newPacket());
        assertNull(q.get());
        assertTrue(q.isEmpty());
    }

    @Test
    public void testRemove() {
        Packet p = newPacket();
        q.add(p);
        q.remove(p);
        assertNull(q.get());
        assertTrue(q.isEmpty());
    }

    @Test
    public void testOverflow() {
        Packet p1 = newPacket();
        Packet p2 = newPacket();
        Packet p3 = newPacket();
        Packet p4 = newPacket();
        q.add(p1);
        assertFalse(q.isFull());
        q.add(p2);
        assertFalse(q.isFull());
        q.add(p3);
        assertTrue(q.isFull());
        q.add(p4);
        assertTrue(q.isFull());
        assertFalse(q.isEmpty());
        assertEquals(p2, q.get());
        q.remove(p2);
        assertEquals(p3, q.get());
        q.remove(p3);
        assertEquals(p4, q.get());
        q.remove(p4);
        assertNull(q.get());
        assertTrue(q.isEmpty());
    }

    @Test
    public void testTimeout() throws InterruptedException {
        Packet p1 = newPacket();
        q.add(p1);
        Thread.sleep(timeout*2);
        assertFalse(q.isEmpty());
        assertNull(q.get());
        assertTrue(q.isEmpty());
        Packet p2 = newPacket();
        q.add(p2);
        assertEquals(p2, q.get());
        assertFalse(q.isEmpty());
    }

    private Packet newPacket() {
        ByteBuffer b = ByteBuffer.allocate(4);
        b.putInt(seq++);
        b.flip();
        return Packet.builder().putData(b).build();
    }
}